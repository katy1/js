# -*- coding: utf8 -*-
from page.PageSearch import PageSearch


class PageMethodSearch(object):
    page_search = PageSearch()

    def width_field(self, driver):
        find_element_search = self.page_search.find_element(
            driver, self.page_search._locators['element_search'])
        width = driver.execute_script(
            "return arguments[0].clientWidth;", find_element_search)
        print width

    def identify_scroll(self, driver):
        height_windows = driver.execute_script("return window.innerHeight;")
        find_element_body = self.page_search.find_element(
            driver, self.page_search._locators['element_body'])
        height_scroll = driver.execute_script(
            "return arguments[0].clientHeight;", find_element_body)
        if (height_scroll != height_windows):
            print "Скрол есть на странице"
        else:
            print "Скрол отсутсвует на странице"

    def delete_focus(self, driver):
        find_element_serch = self.page_search.find_element(
            driver, self.page_search._locators['element_search'])
        driver.execute_script(
            "return arguments[0].blur();", find_element_serch)
