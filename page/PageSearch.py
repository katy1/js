# -*- coding: utf8 -*-
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


class PageSearch(object):
    _locators = {
        'element_search': "lst-ib",
        'element_body': "gsr",
        'element_result': "rcnt"
    }

    @staticmethod
    def find_element(driver, path_element):
        WebDriverWait(driver, 30).until(EC.presence_of_element_located((
            By.ID, path_element)))
        return driver.find_element_by_id(path_element)

    def set_value_field(self, driver, value):
        self.find_element(
            driver, self._locators['element_search']).send_keys(value)

    def enter_search(self, driver):
        self.find_element(
            driver, self._locators['element_search']).send_keys(Keys.ENTER)

    def result_search(self, driver):
        self.find_element(driver, self._locators['element_result'])
