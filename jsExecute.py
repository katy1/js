import unittest
from selenium import webdriver
from page.PageSearch import PageSearch
from PageMethodSearch import PageMethodSearch


class TestCase(unittest.TestCase):
    def setUp(self):
        self.page_search = PageSearch()
        self.page_method_search = PageMethodSearch()
        self.driver = webdriver.Firefox()
        self.driver.get("https://www.google.ru/")

    def tearDown(self):
        self.driver.quit()

    def test_js(
            self):
        driver = self.driver
        self.page_method_search.width_field(driver)
        self.page_method_search.identify_scroll(driver)
        self.page_search.set_value_field(driver, "javascript")
        self.page_search.enter_search(driver)
        self.page_search.result_search(driver)
        self.page_method_search.identify_scroll(driver)


if __name__ == '__main__':
    unittest.main()
